variable domain {}
variable unit {}
variable environment {}
variable project {}
variable k8s_namespace {}
variable secret_name {}
variable secret_data {
  type = map(string)
}
variable "secret_type" {
  default = "Opaque"
}
