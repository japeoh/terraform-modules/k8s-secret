resource kubernetes_secret secret {
  metadata {
    name      = var.secret_name
    namespace = var.k8s_namespace
    labels    = local.labels
  }

  data = var.secret_data

  type = var.secret_type
}
